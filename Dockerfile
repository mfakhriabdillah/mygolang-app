FROM golang:alpine3.16
WORKDIR /app
COPY . .
RUN go mod download
RUN go build -o goapp
EXPOSE 8080
ENTRYPOINT ["/app/goapp"]